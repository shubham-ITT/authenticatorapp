/*jslint node: true, nomen: true, plusplus: true*/
'use strict';

require('winston-daily-rotate-file');

var winston = require('winston'),
    fs = require('fs'),
    env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
    config = require('./config.js')[env];


winston.loggers.add('logger', {
  transports: [
    new (winston.transports.Console)(
        {
          level: config.debugLogLevel,
          colorize: true
        }),

    //new files will be generated each day, the date patter indicates the frequency of creating a file.
    new winston.transports.DailyRotateFile({
          name: 'debug-log',
          filename: config.debugLogFileName,
          level: config.debugLogLevel,
          prepend: true,
          datePattern: config.datePattern,
          maxFiles: config.maxFiles
        }
    ),
    new (winston.transports.DailyRotateFile)({
      name: 'error-log',
      level: config.errorLogLevel,
      filename: config.errorLogFilename,
      prepend: true,
      datePattern: config.datePattern,
      maxFiles: config.maxFiles
    })
  ]
});


//creates a directory if the directory is not present
exports.createLogDirectory = function () {
  if (!fs.existsSync(config.logDirectory)) {
    fs.mkdirSync(config.logDirectory);
  }
}

var customLogger = winston.loggers.get('logger');
Object.defineProperty(exports, "LOG", {value: customLogger});
