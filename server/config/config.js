/*jslint node: true, nomen: true, plusplus: true*/
'use strict';

var path = require('path');
var rootPath = path.normalize(__dirname + '/../../');


module.exports = {
  development: {
    port: 3012,
    swaggerBaseUrl: "https://localhost",
    rootPath: rootPath,
    apiPaths: {
      version1: '/api/v1',
      excludeAuthentication: ['/api/v1/auth/login', '/api/v1/auth/register']
    },
    excludeLogApi: ['/api/v1/auth/login', '/api/v1/auth/register'],
    logDirectory: rootPath + '/logs',
    errorLogLevel: 'error',
    errorLogFilename: rootPath + '/logs/_error.log',
    debugLogLevel: 'debug',
    debugLogFileName: rootPath + '/logs/_debug.log',
  }
}